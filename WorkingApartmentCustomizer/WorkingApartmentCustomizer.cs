﻿// Mod for My Summer Car to customize WorkingApartment version 1.0.5.
// Copyright (C) 2024 Akatsuki
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using MSCLoader;
using UnityEngine;
using WorkingApartmentCustomizer.Features;

namespace WorkingApartmentCustomizer
{
    public class WorkingApartmentCustomizer : Mod
    {
        private SettingsCheckBox _removeCornerTable;
        private SettingsCheckBox _removeWorkTable;
        private SettingsCheckBox _removeShelves;
        private SettingsCheckBox _removeMidTable;
        public override string ID => "WorkingApartmentCustomizer";
        public override string Version => "1.0";
        public override string Author => "アカツキ";

        public override void ModSetup()
        {
            base.ModSetup();

            SetupFunction(Setup.PostLoad, On_Load);
        }

        public override void ModSettings()
        {
            base.ModSettings();

            Settings.AddText(this,
                "If you want to install the textures, drop the textures files into the assets folder of the mod.");
            Settings.AddText(this, $"For you, the assets folder is located in: {ModLoader.GetModAssetsFolder(this)}");
            Settings.AddText(this, "All of the following items are valid: GaragePoster.png, Poster1.png, Poster2.png, Painting_Hallway.png, Painting_Bedroom.png, Painting_LivingRoom.png, Painting_Kitchen.png and Painting_Bedroom2.png.");

            _removeCornerTable = Settings.AddCheckBox(this, "Remove Corner Table", "Remove the corner table in the garage.", false);
            _removeWorkTable = Settings.AddCheckBox(this, "Remove Work Table", "Remove the work table in the garage.", false);
            _removeShelves = Settings.AddCheckBox(this, "Remove Shelves", "Remove the shelves in the garage.", false);
            _removeMidTable = Settings.AddCheckBox(this, "Remove Middle Table", "Remove the middle table in the garage.", false);

            Settings.AddButton(this, "Developer's Website", () =>
            {
                Application.OpenURL("https://akatsuki.nekoweb.org/");
            });

            Settings.AddButton(this, "Suggestions", () =>
            {
                Application.OpenURL("https://akatsuki.nekoweb.org/suggestions");
            });
        }

        private void On_Load()
        {
            FlagReplace.ApplyFlagReplace(this);
            PosterReplace.ApplyPosterReplace(this);
            PictureReplace.ApplyPictureReplace(this);

            GarageRemove.RemoveCornerTable(_removeCornerTable);
            GarageRemove.RemoveWorkTable(_removeWorkTable);
            GarageRemove.RemoveShelves(_removeShelves);
            GarageRemove.RemoveMiddleTable(_removeMidTable);
        }
    }
}