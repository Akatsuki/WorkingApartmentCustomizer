﻿// Mod for My Summer Car to customize WorkingApartment version 1.0.5.
// Copyright (C) 2024 Akatsuki
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.IO;
using MSCLoader;
using UnityEngine;

namespace WorkingApartmentCustomizer.Features
{
    public static class PosterReplace
    {
        public static void ApplyPosterReplace(Mod mod)
        {
            Poster1(mod);
            Poster2(mod);
            GaragePoster(mod);
        }

        private static void GaragePoster(Mod mod)
        {
            if (!File.Exists(Path.Combine(ModLoader.GetModAssetsFolder(mod), "GaragePoster.png")))
                return;

            var garagePoster = GameObject.Find("Garage(Clone)/poster1");

            garagePoster.GetComponent<Renderer>().material = new Material(Shader.Find("Standard"))
            {
                mainTexture = LoadAssets.LoadTexture(mod, "GaragePoster.png")
            };
        }

        private static void Poster1(Mod mod)
        {
            if (!File.Exists(Path.Combine(ModLoader.GetModAssetsFolder(mod), "Poster1.png")))
                return;

            var poster1Go = GameObject.CreatePrimitive(PrimitiveType.Cube);
            // Local Position (X,Y,Z): -1283.776f, 1.909804f, 1077.576f; - Local Euler Angles (X,Y,Z): 11f, 122f, 0f; - Local Scale (X,Y,Z): 0.009999998f, 0.84f, 0.63f
            poster1Go.transform.position = new Vector3(-1283.776f, 1.909804f, 1077.576f);
            poster1Go.transform.rotation = Quaternion.Euler(11f, 122f, 0f);
            poster1Go.transform.localScale = new Vector3(0.009999998f, 0.84f, 0.63f);
            poster1Go.GetComponent<Renderer>().material = new Material(Shader.Find("Standard"))
            {
                mainTexture = LoadAssets.LoadTexture(mod, "Poster1.png")
            };
        }

        private static void Poster2(Mod mod)
        {
            if (!File.Exists(Path.Combine(ModLoader.GetModAssetsFolder(mod), "Poster2.png")))
                return;

            var poster2Go = GameObject.CreatePrimitive(PrimitiveType.Cube);
            // Local Position (X,Y,Z): -1283.826f, 1.909804f, 1079.246f; - Local Euler Angles (X,Y,Z): 352f, 212f, 0f; - Local Scale (X,Y,Z): 0.009999998f, 0.84f, 0.63f
            poster2Go.transform.position = new Vector3(-1283.826f, 1.909804f, 1079.246f);
            poster2Go.transform.rotation = Quaternion.Euler(352f, 212f, 0f);
            poster2Go.transform.localScale = new Vector3(0.009999998f, 0.84f, 0.63f);
            poster2Go.GetComponent<Renderer>().material = new Material(Shader.Find("Standard"))
            {
                mainTexture = LoadAssets.LoadTexture(mod, "Poster2.png")
            };
        }
    }
}