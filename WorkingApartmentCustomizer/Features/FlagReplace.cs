﻿// Mod for My Summer Car to customize WorkingApartment version 1.0.5.
// Copyright (C) 2024 Akatsuki
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.IO;
using MSCLoader;
using UnityEngine;

namespace WorkingApartmentCustomizer.Features
{
    public static class FlagReplace
    {
        public static void ApplyFlagReplace(WorkingApartmentCustomizer mod)
        {
            if (!File.Exists(Path.Combine(ModLoader.GetModAssetsFolder(mod), "Flag.png")))
                return;

            var customFlag = LoadAssets.LoadTexture(mod, "Flag.png");

            GameObject.Find("Garage(Clone)").transform.GetChild(5).gameObject
                .GetComponent<Renderer>().sharedMaterial = new Material(Shader.Find("Standard"))
            {
                mainTexture = customFlag
            };
        }
    }
}