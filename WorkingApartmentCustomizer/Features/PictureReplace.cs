﻿// Mod for My Summer Car to customize WorkingApartment version 1.0.5.
// Copyright (C) 2024 Akatsuki
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.IO;
using MSCLoader;
using UnityEngine;

namespace WorkingApartmentCustomizer.Features
{
    public static class PictureReplace
    {
        public static void ApplyPictureReplace(Mod mod)
        {
            Hallway(mod);
            Bedroom(mod);
            LivingRoom(mod);
            Kitchen(mod);
            Bedroom2(mod);
        }

        private static void Hallway(Mod mod)
        {
            if (!File.Exists(Path.Combine(ModLoader.GetModAssetsFolder(mod), "Painting_Hallway.png")))
                return;

            var newCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            newCube.name = "Painting_Hallway";
            newCube.transform.position = new Vector3(-1284.146f, 1.939947f, 1078.997f);
            newCube.transform.rotation = Quaternion.Euler(0f, 32f, 0f);
            newCube.transform.localScale = new Vector3(0.01000002f, 0.64f, 1f);

            newCube.GetComponent<Renderer>().material = new Material(Shader.Find("Standard"))
            {
                mainTexture = LoadAssets.LoadTexture(mod, "Painting_Hallway.png")
            };
        }

        private static void Bedroom(Mod mod)
        {
            if (!File.Exists(Path.Combine(ModLoader.GetModAssetsFolder(mod), "Painting_Bedroom.png")))
                return;
            var newCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            newCube.name = "Painting_Bedroom";
            newCube.transform.position = new Vector3(-1281.496f, 1.879804f, 1078.656f);
            newCube.transform.rotation = Quaternion.Euler(0f, 211.8798f, 0f);
            newCube.transform.localScale = new Vector3(0.00999999f, 0.8f, 0.6f);

            newCube.GetComponent<Renderer>().material = new Material(Shader.Find("Standard"))
            {
                mainTexture = LoadAssets.LoadTexture(mod, "Painting_Bedroom.png")
            };
        }

        private static void LivingRoom(Mod mod)
        {
            if (!File.Exists(Path.Combine(ModLoader.GetModAssetsFolder(mod), "Painting_LivingRoom.png")))
                return;
            var newCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            newCube.name = "Painting_LivingRoom";
            newCube.transform.position = new Vector3(-1283.136f, 1.749804f, 1076.116f);
            newCube.transform.rotation = Quaternion.Euler(0f, 212f, 0f);
            newCube.transform.localScale = new Vector3(0.01f, 0.6599999f, 1.02f);

            newCube.GetComponent<Renderer>().material = new Material(Shader.Find("Standard"))
            {
                mainTexture = LoadAssets.LoadTexture(mod, "Painting_LivingRoom.png")
            };
        }

        private static void Kitchen(Mod mod)
        {
            if (!File.Exists(Path.Combine(ModLoader.GetModAssetsFolder(mod), "Painting_Kitchen.png")))
                return;
            var newCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            newCube.name = "Painting_Kitchen";
            newCube.transform.position = new Vector3(-1289.259f, 1.763804f, 1079.595f);
            newCube.transform.rotation = Quaternion.Euler(0f, 212f, 0f);
            newCube.transform.localScale = new Vector3(0.01f, 0.6599999f, 1.02f);

            newCube.GetComponent<Renderer>().material = new Material(Shader.Find("Standard"))
            {
                mainTexture = LoadAssets.LoadTexture(mod, "Painting_Kitchen.png")
            };
        }

        private static void Bedroom2(Mod mod)
        {
            if (!File.Exists(Path.Combine(ModLoader.GetModAssetsFolder(mod), "Painting_Bedroom2.png")))
                return;
            var newCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            newCube.name = "Painting_Bedroom2";
            newCube.transform.position = new Vector3(-1287.449f, 1.803804f, 1082.525f);
            newCube.transform.rotation = Quaternion.Euler(0f, 212f, 0f);
            newCube.transform.localScale = new Vector3(0.01f, 0.6599999f, 1.02f);

            newCube.GetComponent<Renderer>().material = new Material(Shader.Find("Standard"))
            {
                mainTexture = LoadAssets.LoadTexture(mod, "Painting_Bedroom2.png")
            };
        }
    }
}