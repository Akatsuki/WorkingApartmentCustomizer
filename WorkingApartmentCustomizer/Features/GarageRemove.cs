﻿// Mod for My Summer Car to customize WorkingApartment version 1.0.5.
// Copyright (C) 2024 Akatsuki
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using MSCLoader;
using UnityEngine;

namespace WorkingApartmentCustomizer.Features
{
    public static class GarageRemove
    {
        public static void RemoveCornerTable(SettingsCheckBox checkBox)
        {
            if (!checkBox.GetValue()) return;
            var cornerTable = GameObject.Find("Garage(Clone)/table2");
            Object.Destroy(cornerTable);
        }

        public static void RemoveWorkTable(SettingsCheckBox checkBox)
        {
            if (!checkBox.GetValue()) return;
            var workTable = GameObject.Find("Garage(Clone)/table1");
            Object.Destroy(workTable);
        }

        public static void RemoveShelves(SettingsCheckBox checkBox)
        {
            if (!checkBox.GetValue()) return;
            var shelves = GameObject.Find("Garage(Clone)/shelves");
            Object.Destroy(shelves);
            var boxes = GameObject.Find("Garage(Clone)/boxes");
            Object.Destroy(boxes);
        }

        public static void RemoveMiddleTable(SettingsCheckBox checkBox)
        {
            if (!checkBox.GetValue()) return;
            var drawer = GameObject.Find("Garage(Clone)/drawer");
            Object.Destroy(drawer);
        }
    }
}