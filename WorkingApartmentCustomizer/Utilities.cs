﻿// Mod for My Summer Car to customize WorkingApartment version 1.0.5.
// Copyright (C) 2024 Akatsuki
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using UnityEngine;

namespace WorkingApartmentCustomizer
{
    public static class Utilities
    {
        private static Transform C(this Transform t, int i)
        {
            return t.GetChild(i);
        }

        public static GameObject C(this GameObject go, int i)
        {
            return go.transform.C(i).gameObject;
        }
    }
}